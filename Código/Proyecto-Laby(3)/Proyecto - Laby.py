## ~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~ ##
## Programa: Laby.py                                                         ##
## Autores : Samantha Arburola, Luis Rojas                                   ##
## Fechas  : 24.06.2014                                                      ##
##                                                                           ##
##    Este programa al ejecutarse premite jugar en un laberinto, el cual es  ##
## escogido mediaante un archivo .lab El Archivo contiene la especificación  ##
## del formato, los bloques y ratón.                                         ##
##    El juego consiste es alcanzar los bloques rojos para ganar, si en el   ##
## camino choca con un bloque color amarillo el ratón morirá y perderá la    ##
## jugada, los bloques de cualquier otro color simplemente serán objetos     ##
## que no puede traspasar el ratón.                                          ##
## ~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~..~~~ ##

##### IMPORTACIONES #####
import tkinter          #
from tkinter import *   #
from tkinter import Tk  #
from tkinter import ttk #
import winsound         #
#########################

####### GLOBALES ########
Laby = {}               #
LabyLista=[]            #
archivo = ""
Radio = None
#########################

## Funcion de juego
def pierde():
    """ Ventana al chocar con amarillo
        Es recurso del método keepjuega
    """
    ## Funcion de Botones
    def Intento():
        ins.withdraw()
        #print("intenta",archivo)
        q = Archivo(archivo)
    ## Crea la ventana.
    ins = tkinter.Tk()
    ins.config(bg = "black")
    ins.geometry("200x100+600+300") ## Ancho, Largo, X Y (posiciona ventana pantalla)
    ins.title("ATENCION")

    ## Texto: Título.
    titulo = tkinter.Label(ins, text = "Laby ha sido capturado.\n ¿Deseas intentarlo nuevamente?\n", font = "Times 12")
    titulo.place(x = 0, y = 10)
    titulo.config(bg = "black")
    titulo.config(fg = "white")

    ## Boton: Intentar.
    botonre = tkinter.Button(ins, text = "▂ ▃ Intentar ▃ ▂", font = "Times 10",command = Intento)
    botonre.place(x = 3, y = 62)
    botonre.config(bg = "black")
    botonre.config(fg = "white")

    ## Boton: Menu.
    botonre = tkinter.Button(ins, text = "▂ ▃   Menu   ▃ ▂", font = "Times 10",command = lambda:ins.destroy())
    botonre.place(x = 103, y = 62)
    botonre.config(bg = "black")
    botonre.config(fg = "white")

    sonido= "die.wav"
    winsound.PlaySound(sonido, winsound.SND_FILENAME|winsound.SND_ASYNC)
    
def gana():
    """ Ventana al chocar con rojo
        Es recurso del método keepjuega
    """
    """ Ventana al chocar con rojo """
    ## Funcion de Botones
    def Intento():
        ins.withdraw()
        #print("intenta",archivo)
        q = Archivo(archivo)
    
    ## Crea la ventana.
    ins = tkinter.Tk()
    ins.config(bg = "black")
    ins.geometry("200x100+600+300") ## Ancho, Largo, X Y (posiciona ventana pantalla)
    ins.title("ATENCION")
            
    ## Texto: Título.
    titulo = tkinter.Label(ins, text = "Laby ha llegado al queso.\n ¿Deseas intentarlo nuevamente?"  , font = "Times 12")
    titulo.place(x = 0, y = 10)
    titulo.config(bg = "black")
    titulo.config(fg = "white")

    ## Boton: Intentar.
    botonre = tkinter.Button(ins, text = "▂ ▃ Intentar ▃ ▂", font = "Times 10",command = Intento)
    botonre.place(x = 3, y = 62)
    botonre.config(bg = "black")
    botonre.config(fg = "white")

    ## Boton: Menu.
    botonre = tkinter.Button(ins, text = "▂ ▃   Menu   ▃ ▂", font = "Times 10",command = lambda:ins.destroy())
    botonre.place(x = 103, y = 62)
    botonre.config(bg = "black")
    botonre.config(fg = "white")

    sonido= "win.wav"
    winsound.PlaySound(sonido, winsound.SND_FILENAME|winsound.SND_ASYNC)

            
" ===================== DEFINICIÓN DE LA CLASE LABERINTO ===================== "
class Laberinto():
    """ Crea un laberinto.
        Esta clase contiene los métodos para crear un laberinto.
    """
    def __init__(self, alto="600", ancho="600", fondo="gray"):
        """ Crea una ventana vacía para el laberinto
            Entradas:
                alto,  medida del alto del Tk.
                ancho, medida del ancho del Tk.
                fondo, collor de fondo pata el Tk.
            Salida:
                Dibuja un Tk y un Canvas para en él dibujar bloques
                y el ratón para el juego.
            Restricciones:
                Ninguna.
        """
        ## Define las propiedades
        self.alto  = alto
        self.ancho = ancho
        self.fondo = fondo
        ## Crea el Tk
        global juega
        juega = Tk()
        self.juega = juega
        self.juega.geometry(self.ancho+"x"+self.alto)
        self.juega.title("Bienvenid@ a Laby")
        self.lastx = 0
        self.lasty = 0
        ## Crea el Canvas
        lab = Canvas(self.juega, width=self.ancho, height=self.alto, bg =self.fondo)
        self.lab = lab
        self.lab.pack()
    ########### BLOQUE ###############
    def bloque(self, x1, x2, y1, y2, color="yellow"):
        """ Crea un bloque en el laberinto.
            Entradas:
                x1, y1, x2, y2, corresponden a la ubicación
                del bloque en el Canvas.
                color, define el color del bloque.
                Si el usuario no define el color del bloque este será amarillo.
            Salidas:
                Dibuja un bloque en la posición y color respectivo.
            Restricciones:
                Ninguna.
        """
        ## Define la posición y Color
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.color = color
        ## Crea el bloque
        self.lab.create_rectangle(self.x1,self.y1,self.x2,self.y2,fill=self.color)

    ########### RATON ##############
    ## Funciones para el movimiento del ratón.
    ## Especificación de acciones a ejecutar por eventos.
    def iniciaMovimiento(self, event):
        # Recuerda la posición de inicio del objeto a mover
        dentro = 0 <= event.x + Radio <= int(self.ancho) and 0 <= event.y + Radio <= int(self.alto)
        if dentro:
            self.lastx = event.x
            self.lasty = event.y
        else:
            self.lastx = event.x-Radio
            self.lasty = event.y-Radio
            
        

        ########## MOVIMIENTO  #############
        ## Mueve el ratón y evalua la posición jugada
    def mueveRaton(self, event):
        # Mueve el ratón.
        dentro = 0 <= event.x <= int(self.ancho) and 0 <= event.y <= int(self.alto)
        if dentro:
            self.lab.move(self.raton, event.x - self.lastx, event.y - self.lasty)

        else:
            self.lastx = event.x-Radio
            self.lasty = event.y-Radio
            
        self.lastx = event.x
        self.lasty = event.y
        ## POSICION DEL RATON A EVALUAR
        print("X", self.lastx, "Y", self.lasty)

        """ Evalua la posición y autoriza movimiento.
        Entrada:
            Laby, diccionario que contiene los bloques y propiedades del laberinto.
        Salida:
            True, si puede moverse.
            False, si choca con un bloque.
        Restricciones:
            Ninguna.
        """
        ## Compara el self.eventx y self.eventy, si estan en territorio de un bloque,
        ## Revisar los territorios de un diccionario
        estado = ""
        for prop in Laby:
            if "bloque" in prop:
                #print(Laby[prop])
                ## Asigna los valores según la definición de cada bloque
                ## Compara y elige cual x es mayor y menor
                ex1 = Laby[prop]['x1'] if Laby[prop]['x1'] < Laby[prop]['y1'] else Laby[prop]['y1']
                ex2 = Laby[prop]['x1'] if Laby[prop]['x1'] > Laby[prop]['y1'] else Laby[prop]['y1']
                ey1 = Laby[prop]['y2'] if Laby[prop]['y2'] < Laby[prop]['x2'] else Laby[prop]['x2']
                ey2 = Laby[prop]['y2'] if Laby[prop]['y2'] > Laby[prop]['x2'] else Laby[prop]['x2']
                ## Si la "x" esta dentro del rango de x1 a x2, lo reporta            
                if (ex1 <= self.lastx <= ex2) and (ey1 <= self.lasty <= ey2):
                    print("choque")
                    ##Acciones a tomar si el bloque es rojo.
                    if Laby[prop]['color'] == "red":
                        self.lastx=0
                        self.lasty=0
                        self.juega.withdraw()
                        estado = "G"
                        break
                    ##Acciones a tomar si el bloque es amarillo    
                    elif Laby[prop]['color'] == "yellow":
                        self.lastx=0
                        self.lasty=0
                        self.juega.withdraw()
                        estado = "P"
                        break
                    ##Acciones a tomar si el bloque es otro color 
                    elif Laby[prop]['color'] != ("yellow" or "red"):
                        sonido= "choca.wav"
                        winsound.PlaySound(sonido, winsound.SND_FILENAME|winsound.SND_ASYNC)
                        
        if estado == "G": gana()
        elif estado == "P": pierde()
                        

        ## fin mueve raton ##
        
                
    def ingreso(self, event):
        # Cambia el color cuando el mouse ingresa al objeto (enter).
        self.lab.itemconfig(self.raton, fill="red")

    def salida(self, event):
        # Cambia el color cuando el mouse sale del objeto (leave).
        self.lab.itemconfig(self.raton, fill="blue")

    ## Crea el ratón
    def raton(self,x=0,y=0,radio=15):
        """ Crea el ratón.
            Entradas:
                x, y, corresponden a la posición de inicio del ratón en el Canvas.
                radio, define el tamaño del ratón.
            Salida:
                Dibuja el ratón en el Canvas; en efecto en el Laberinto.
            Restricciones:
                Ninguna.
            
        """
        self.radio = radio
        # Crea el raton
        raton = self.lab.create_oval(x,y,radio,radio,fill="green", tags="selected")
        self.raton = raton
        # Realiza el bind de eventos que permiten mover el ratón
        self.lab.tag_bind(self.raton, "<1>", self.iniciaMovimiento)
        self.lab.tag_bind(self.raton, "<B1-Motion>", self.mueveRaton)

    ########## LIMITE  #########################################################################################################################

    def esquinas(self):
        '''Regresa una tupla (x1,y1,x2,y2).

        x1, y1, x2, y2 definen un cuadrado mediante dos
        de sus esquinas:

        x1, y1: coordenadas de la esquina superior izquierda
        x2, y2: coordenadas de la esquina inferior derecha

        El cuadrado contiene un círculo con centro en x,y,
        su dimensión dada por el atributo de clase Radio.
        '''

        limites = (((event.x - Radio >= 0) and (event.x + Radio) <= ancho) \
                  and (event.y - Radio >= 0) and  ((event.x + Radio) <= ancho))
        if limites:
            self.lab.move(self.raton, event.x - self.lastx, event.y - self.lasty)
            self.lastx = event.x
            self.lasty = event.y

""" ---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~ """
class Archivo(Laberinto):
    """ Carga el archivo .lab
        Esta es una sub clase de Laberinto que carga el archivo y procesa la
        información para dibujar el laberinto.
        Entrada:
            Laberinto, clase Laberinto.
        Salida:
            Dibujo del Laberinto definido en el archivo.
        Restricciones:
            Ninguna.
    """
    def crea(self):
        """ Envia al canvas los parametros del archivo"""
        global ven_Juega, Laby, Radio
        ## Establece las propiedades del Canvas y Ratón
        for sub in self.lista:
            if sub[0] == "ancho":
                a = sub[1]
            elif sub[0] == "alto":
                b = sub[1]
            elif sub[0] == "fondo":
                c = sub[1]
            elif sub[0] == "raton":
                ##print(sub)
                raton = {"x":(int(sub[1]) if sub[1].isdigit() else 0),
                         "y":(int(sub[2]) if sub[2].isdigit() else 0),
                         "radio":(int(sub[3]) if sub[3].isdigit() else 0)}
                ##print(raton)
                Laby['raton'] = raton
                Radio = raton["radio"]
        ## Crea el Laberinto

        vent_Juega = Laberinto(ancho=a, alto=b, fondo=c)
        Laby['propiedades'] = {'ancho': a, 'alto': b, 'fondo': c }
        ## Crea el ratón
        fifi = vent_Juega.raton(x=raton['x'], y=raton['y'],radio=raton['radio'])
        ## Crea los bloques
        i = 0
        b = 0
        while i < len(self.lista):
            ## Crea cada bloque definido en el archivo .lab
            ##print(self.lista[i])
            if self.lista[i][0] == "bloque":
                bloque= {"x1": (int(self.lista[i][1]) if self.lista[i][1].isdigit() else 0),
                         "x2": (int(self.lista[i][2]) if self.lista[i][2].isdigit() else 0),
                         "y1": (int(self.lista[i][3]) if self.lista[i][3].isdigit() else 0),
                         "y2": (int(self.lista[i][4]) if self.lista[i][4].isdigit() else 0),
                         "color": ("yellow" if len(self.lista[i]) <= 5 else self.lista[i][5])}
                vent_Juega.bloque(bloque['x1'],bloque['y1'],bloque['x2'],bloque['y2'],bloque['color'])
                Laby["bloque%d" % b] = bloque
                b += 1
##            ## Si no hay bloques definidos en el archivo crea uno de meta
##            if not((self.lista[i] != "ancho") and (self.lista[i] != "alto")):
##                if not((self.lista[i] != "bloque") and (self.lista[i] != "fondo")):
##                    vent_Juega.bloque(x1=(str(int(a)/2)),y1=(str(int(a)/2)),
##                                  x2=(str(int(b)/2)), y2=(str(int(b)/2)), color="red")
            i += 1
        
    def __init__(self, archivo):
        """ Leer un arcivho .lab
            Entrada:
                archivo, nombre del archivo .lab
            Salida:
                lista, conntiene sublistas con cada línea del archivo .lab
            Verificaciones:
                Que el archivo exista.
        """
        global ven_Laby , LabyLista  ## Globales por modificar
        self.archivo = archivo ## Define el objeto
        ## Verificacion
        if FileNotFoundError :
            ven_Laby.sal()
            ventana_menu()
        ## Abre el archivo
        apertura = open(str(self.archivo) + ".lab","r")
        ## Lee el archivo y guarda en una lista cada línea
        self.lista = []
        linea = apertura.readline()
        while linea:
            self.lista.append(linea.split())
            linea = apertura.readline()
        ## Quita vacíos
        i = 0
        while i < len(self.lista):
            if (self.lista[i] == []) or (self.lista[i] == ['']) or (len(self.lista[i]) == (1 or 0)): self.lista.remove(self.lista[i])
            elif self.lista[-1] == [''] or self.lista[-1] == []: self.lista.remove(self.lista[-1])
            i += 1
        ## Salida
        ## return lista
        ## Modifica Global
        LabyLista = self.lista
        ## Próximo Procedimiento
        self.crea()

""" ---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~ """
                        
" ============ DEFINICIÓN DE LA CLASE LabyMenu PARA EL LABERINTO ============= "
class LabyMenu():
    """ Crea la ventana de menú para cargar el laberinto """
    def __init__(self):
        """ Crea una ventana para menú con las opciones:
                 - Introducir un archivo .lab 
                 - Limpia el laberinto.
                 - Reiniciar la jugada y se mantiene el archivo.
        """
        ## Crea la ventana Tk
        self.ventana = Tk()
        self.ventana.config(bg = "black")
        self.ventana.geometry ("200x112+600+300") ## Ancho, Largo, X Y (posiciona ventana pantalla)
        self.ventana.title ("LABY")
        ############  MENU   ############
        ## Bienvenida
        self.bienvenida = Label(self.ventana,text = "@@@@@@ LABY @@@@@@", font = "Century 11")
        self.bienvenida.config(bg = "black", fg = "white")
        self.bienvenida.pack()
        
    def archivo(self):
        def archi_entra():
            """ Función auxiliar para lee el archivo digitado.
                Entrada:
                    Ninguna.
                Salida:
                    Ninguna.
                Restricción:
                    Ninguna.
            """
            ## Envía a la entrada del menú a procesarse
            ## guarda el la global para reiniciar
            global archivo
            archivo = str(e1.get())
            ("archivo",archivo)
            q = Archivo(e1.get())
            self.ventana.withdraw()
            self.ventana.destroy()
           
            
        
        ## CREAR ESPACIO PARA ESCRIBIR
        self.entrada = Label(self.ventana, text = ">>>  Archivo Laberinto  <<<", font = "Times 11")
        self.entrada.place(x = 11, y = 25)
        self.entrada.config(bg = "black", fg = "white")
        #self.entrada.pack()
        
        ## ENTRADA DE ARCHIVO .LAB
        e1 = Entry(self.ventana)
        e1.place(x = 10, y = 57)
        
        ## Boton ok!
        self.ok = tkinter.Button(self.ventana, text = "¡OK!", font = "Rockwell 10", relief = "ridge", command = archi_entra, cursor="spider")
        self.ok.place(x = 150, y = 52)
        self.ok.config(bg = "black", fg = "white")

        ## Decoración        
        self.entrada = Label(self.ventana, text = "@@@@@@ LABY @@@@@@", font = "Century 11")
        self.entrada.place(x = 5, y = 85)
        self.entrada.config(bg = "black", fg = "white")
        
    def sal(self):
        """ Cerrar la ventana """
        self.ventana.withdraw()
""" ---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~ """    

""" === FUNCIÓN: VENTANA MENÚ GENERAL INTERACTIVA ========================== """   
def ventana_menu():
        """ Presenta Abrir Archivo, Limpiar, Reiniciar
            Entrada:
                Ninguna.
            Salida:
                Ventana de Menú
            Resticciones:
                Ninguna.
        """
        global ven_Laby
        ven_Laby = LabyMenu()
        ven_Laby.archivo()
        
def menu():
    """ Crea la pantalla principal del Juego
        con un enu en cascada que posee las
        opciones de Jugada, inicio y salida
    """
    ## ++ ##
    def instructivo():
        """ Explica el objetivo del juego """
        ## Crea la ventana.
        ins = tkinter.Tk()
        ins.config(bg = "black")
        ins.geometry("420x270+600+300") ## Ancho, Largo, X Y (posiciona ventana pantalla)
        ins.title("¿Como jugar con Laby?")

        ## Texto: Título.
        titulo = tkinter.Label(ins, text = "INSTRUCCIONES" , font = "Impact 12")
        titulo.place(x = 165, y = 25)
        titulo.config(bg = "black")
        titulo.config(fg = "white")

        ## Texto: Explicación.
        titulo = tkinter.Label(ins, text = "¡Corre, Laby, Corre!. Laby es un ratón aventurero que busca queso" + "\n" \
                               "pero el camino esta lleno de obstáculos." + "\n" +" Tu misión es ayudarlo a encontrar su queso."\
                               + "\n" + "\n" + "Evade los bloques Amarillos Vigilantes, y no los toques durante"+ "\n" + \
                               "el recorrido; esquiva los obstaculos que lo destruyen" + "\n" + "\n" \
                               "Corre y llega hasta la meta en Pueblo Rojo," + "\n" + "donde festejarás la victoria con QUESO"\
                               , font = "Times 11")
        titulo.place(x = 10, y = 55)
        titulo.config(bg = "black")
        titulo.config(fg = "white")

        ## Botón para salir.
        botonacepta = tkinter.Button(ins, text = "  CONTINUAR  ", font = "Verdana 10",command=lambda:ins.destroy())
        botonacepta.place(x = 165, y = 228)
        botonacepta.config(bg = "black")
        botonacepta.config(fg = "white")
    ## ++ ##
    def acerca():
        """ Muestra un "acerca" del Programa"""
        ## Crea la ventana.
        vacerca = tkinter.Tk()
        vacerca.geometry("200x200")

        ## Información.
        vacerca.title("Acerca de Laby")
        lbacerca = tkinter.Label(vacerca, text= "Diseñado por "+ "\n" +"Samantha Arburola"+"\n"+"&"+"\n"+"Luis Rojas")
        lbinst = tkinter.Label(vacerca, text= "ITCR"+ "\n" +"Ingeniería de Computación")
        lbversion = tkinter.Label(vacerca, text = "Version 1.1", font= "Papyrus")
        btokacerca = tkinter.Button(vacerca, text= "Continuar", relief = "ridge",command = lambda:vacerca.destroy())

        ## Posición del la información.
        lbversion.place(x=55, y=5)
        lbacerca.place(x=45, y=50)
        lbinst.place(x=25, y=120)
        btokacerca.place(x=65,y=170)

    ###########################################################

    ## Crea la Ventana de Presentación
    root = Tk()
    root.title("Bienvenid@ a Laby")
    bn = ttk.Label()
    inicio = PhotoImage(file=r'inicio.gif')
    root.geometry("%dx%d" %(inicio.width()+5, inicio.height()+5))
    bn['image'] = inicio
    bn.pack()

    ## Barra de menú
    menubar = Menu(root)
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="Iniciar", command = ventana_menu)
    filemenu.add_command(label="Cerrar", command=lambda:root.destroy())

    menubar.add_cascade(label="Juego", menu=filemenu)
    editmenu = Menu(menubar, tearoff=0)

    editmenu.add_command(label="¿Como Jugar?", command = instructivo)

    menubar.add_cascade(label="Reglas", menu=editmenu)
    helpmenu = Menu(menubar, tearoff=0)
    helpmenu.add_command(label="Acerca de...", command = acerca)
    menubar.add_cascade(label="Ayuda", menu=helpmenu)

    root.config(menu=menubar)
    root.mainloop()
""" ---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~---~~~ """
## Run Program
################################################################################
menu()
