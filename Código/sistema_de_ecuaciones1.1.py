##************************************************************************
##
## Programa: sistema_de_ecuaciones1.1.py
## Autor   : Samantha Arburola
## Fecha   : 20.08.2013
##
## Resolver un sistema de ecuaciones e indicar el tipo de sistema;
## si tiene única solución indicarla, reportar si no tiene solución o
## si tiene soluciones infinitas.
##
## Entradas     : a, b, c, a2, b2, c2
##
## Salidas      : tipo de sistema y solución
##
## Restricciones: ninguna
##
##************************************************************************


## Presentación del programa
print("Sistemas de Ecuaciones. Versión 1.0.\n\nBienvenid@")
instrucciones = "Para realizar este cálculo debe:\n" \
                "  1. Utilizar la función sistema_ecuaciones\n" \
                "  2. Escribir todos los números en forma 0.00\n"  
print(instrucciones)
  
## Definición de Función
def sistema_ecuaciones(a, b, c, a2, b2, c2):
    """ Resolver un sistema de ecuaciones

        Entradas     : a, b, c, a2, b2, c2

        Salidas      : Indicar el tipo de sistema y de poseer solución, reportarla.

        Restricciones: las entradas deben ser valores numéricos !=0
    """

    ## Restricciones

    ## Entradas son valores numéricos
    assert isinstance(a, float) and isinstance(b, float)
    assert isinstance(c, float) and isinstance(a2, float)
    assert isinstance(b2, float) and isinstance(c2, float)

    ## Entradas diferentes de cero
    assert a != 0 and b != 0 and c != 0  
    assert a2 != 0 and b2 != 0 and c2 != 0


    ## Cálculo para la ecuación
    
    ## Despeje de x , y
    
    y = (c2-((a2*c)/a)) / ((a2*-b) / a)
    x = (c-2*y) / a


    ## Calculo de resultados de la Ecuación
    calculo_c  = a * x + b * y
    calculo_c2 = a2 * x + b2 * y 


    ## Reporte del resultados

    ## Categorización (compatible, compatible indeterminado, incompatible)
    if (c == calculo_c):
        if (c2 == calculo_c2):
            return("Es un sistema de ecuaciones compatible\nDónde x es ", x, ", y es ", y)
    if (c2 != calculo_c2):
        if (c != calculo_c):
            return("Es un sistema de ecuaciones incompatible")
    else:
        return(("Es un sistema de ecuaciones compatible indeterminado"))


## Lectura de variantes de la Función para el Sistema de Ecuaciones
a  = float(input("Digite a*x : "))
b  = float(input("Digite b*y : "))
c  = float(input("Digite  c  : "))
a2 = float(input("Digite a2*x: "))
b2 = float(input("Digite b2*y: "))
c2 = float(input("Digite  c2 : "))

## Reporte de la Función
print (sistema_ecuaciones(a, b, c, a2, b2, c2))
