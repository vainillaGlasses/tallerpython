#########################################################################
##                                                                     ##
## Instituto Tecnológico de Costa Rica.                                ##
## Escuela de Computación.                                             ##
## Taller de Programación.                                             ##
##                                                                     ##
## I semestre de 2014.                                                 ##
##                                                                     ##
## Examen Parcial                                                      ##
## Profesor: Juan C. Gómez P.                                          ##
##                                                                     ##
## Programa: 2013101697-04.py.                                         ##
## Autor   : Smantha Arburola 2013101697                               ##
"""
##                                                                     ##
## CAMBIOS LINEA 67 y 175                                              ##
##                                                                     ##"""
#########################################################################

import tkinter as tk
from time import sleep

class Ventana():
    """ Esta clase permite ventanas con un canvas en el cual se
        va a dibujar.

           >>
           >> mi_ventana = Ventana("Dibujo", 400, 600)
           >> mi_ventana.canvas.create_line(10, 10, 50, 80, fill="red")
           >>
    """

    def __init__(self, titulo, alto, ancho):
        """ Crea una ventana para dibujar el laberinto.
            Entradas:
                titulo : título de la ventana.
                alto   : alto de la ventana en pixeles.
                ancho  : ancho de la ventna en pixeles.
            Salidas:
                Ninguna.
            Restricciones:
                titulo es una tira, alto y ancho son enteros positivos.
        """
        assert isinstance(titulo, str)
        assert isinstance(alto, int) and alto > 0
        assert isinstance(ancho, int) and ancho > 0

        ## Crea la ventana y un canvas para dibujar
        self.root = tk.Tk()
        self.root.title(titulo)
        self.ancho = ancho
        self.alto = alto
        self.canvas = tk.Canvas(self.root, width=ancho, height=alto)
        self.canvas.pack()

        ## Establece el color de fondo
        self.canvas["bg"] = "white"
        self.canvas.pack()
        
class Raton:
    """ Esta clase permite crear ratones que tienen un
        nombre, posición y dirección."""
    
    def __init__( self, nombre, posicion=(0,0) , direccion='N' ):
        "Constructor"
        self.nombre = nombre
        ##print(posicion)
        self.x, self.y = posicion
        self.direccion = direccion
        self.imagen = None
###########################################################################################################CAMBIOS
    def avanza( self, direccion='' ):
        "Avanza la posición del raton y regresa nueva posición."
        ## Si el parámetro dirección tiene una nueva
        ## dirección, la establece.
        if direccion:
            self.direccion = direccion
        ## Mueve el ratón
        if self.direccion == 'N':    ## Avanza al norte
            self.y = self.y - 1
            self.x = self.x
        elif self.direccion == 'E':  ## Avanza al este
            self.x = self.x + 1
            self.y = self.y
        elif self.direccion == 'S':  ## Avanza al sur
            self.y = self.y + 1
            self.x = self.x
        elif self.direccion == 'O':  ## Avanza al oeste
            self.x = self.x - 1
            self.y = self.y
        elif self.direccion == 'NO':
            self.x = self.x - 1
            self.y = self.y + 1
        elif self.direccion == 'NE':
            self.x = self.x + 1
            self.y = self.y + 1
        elif self.direccion == 'SO':
            self.x = self.x - 1
            self.y = self.y - 1
        elif self.direccion == 'SE':
            self.x = self.x + 1
            self.y = self.y - 1        
            

        ## Retorna la nueva posición del ratón
        return self.x,self.y



class Animacion:
    """ Esta clase permite animar un conjunto de ratones
        que se mueven en forma vertical y horizontal en
        un canvas
    """
    ## Atributos de Clase
    Radio = 5
    Mseg = 50

    @staticmethod
    def esquinas( x, y ):
        '''Regresa una tupla (x1,y1,x2,y2).

        x1, y1, x2, y2 definen un cuadrado mediante dos
        de sus esquinas:

        x1, y1: coordenadas de la esquina superior izquierda
        x2, y2: coordenadas de la esquina inferior derecha

        El cuadrado contiene un círculo con centro en x,y,
        su dimensión dada por el atributo de clase Radio.
        '''

        return x-Animacion.Radio, y-Animacion.Radio, \
               x+Animacion.Radio, y+Animacion.Radio

    def __init__(self, ventana, datos):
        """ Incializa la animación.
            Entradas:
                ventana : ventana sobre la cual se realizará
                          la animación.
                datos   : tira de caracteres que mantiene
                          el nombre de los ratones, su posición
                          inicial, dirección en que se moverá el
                          ratón y su color.
            Salidas:
            Restricciones:
                datos es una tira.
            Supuestos:
                La tira con los datos viene bien conformada.
                ventana es una ventana válida con un canvas.
        """
        self.ratones = []
        self.ventana = ventana

        for especificacion_raton in datos.splitlines():

            ## Obtiene los datos del ratón y crea un ratón
            nombre, x, y, direccion, color = especificacion_raton.split()
            raton = Raton(nombre, (int(x), int(y)),
                          direccion)

            ## Crea el dibujo del ratón
            raton.imagen = ventana.canvas.create_oval(Animacion.esquinas(raton.x,
                                                                         raton.y),
                                                      fill = color)

            ## Agrega a la lista de ratones
            self.ratones.append(raton)

    def mover(self):
        '''Rutina que actualiza posiciones.

        Actualiza las coordenadas de cada ratón y de cada
        círculo.

        En caso de llegar al extremo del área gráfica, se
        invierte la dirección.
        '''
###########################################################################################################CAMBIOS
        for raton in self.ratones:
            # Cambia la posición del ratón en caso de choque
            if raton.x + Animacion.Radio > self.ventana.ancho:# and raton.direccion == 'E':   ## ala izquierda
                raton.avanza('O')
            elif raton.x - Animacion.Radio <= 0:# and raton.direccion == 'O':                 ## a la derecha
                raton.avanza('E')
            elif raton.y + Animacion.Radio > self.ventana.alto:# and raton.direccion == 'S':
                raton.avanza('N')
            elif raton.y - Animacion.Radio <= 0:# and raton.direccion == 'N':
                raton.avanza('S')
            elif (raton.y + Animacion.Radio > self.ventana.alto) and\
            (raton.x + Animacion.Radio > self.ventana.ancho):
                raton.avanza('NO')
            elif (raton.y + Animacion.Radio > self.ventana.alto) and\
            (raton.x - Animacion.Radio <= 0):
                raton.avanza('NE')
            elif (raton.y - Animacion.Radio <= 0) and\
            (raton.x - Animacion.Radio <= 0):
                raton.avanza('SE')
            elif (raton.y - Animacion.Radio <= 0) and\
            (raton.x + Animacion.Radio > self.ventana.ancho):
                raton.avanza('SO')
            elif (raton.x + Animacion.Radio== 0 and raton.y == 0) or \
                 (raton.x + Animacion.Radio== self.ventana.ancho and raton.y - Animacion.Radio== self.ventana.alto):
                if raton.direccion == 'NO':
                    raton.avanza('NE')
                elif raton.direccion == 'NE':
                    raton.avanza('NO')
            elif (raton.x + Animacion.Radio== 0 and raton.y - Animacion.Radio== self.ventana.alto) or \
                 (raton.x + Animacion.Radio== self.ventana.ancho and raton.y - Animacion.Radio== 0):
                if raton.direccion == 'SO':
                    raton.avanza('SE')
                elif raton.direccion == 'SE':
                    raton.avanza('SO')
            elif raton.x + Animacion.Radio== 0 and raton.y -Animacion.Radio == self.ventana.alto:
                if raton.direccion == 'NO':
                     raton.avanza('SE')
                elif raton.direccion == 'SE':
                    raton.avanza('NO')
            elif raton.x +Animacion.Radio > self.ventana.ancho and raton.y -Animacion.Radio==0:
                if raton.direccion == 'SO':
                     raton.avanza('NE')
                elif raton.direccion == 'NE':
                    raton.avanza('SO') 
                
            else:
                raton.avanza()

            # Actualizar la posición del círculo correspondiente:
            self.ventana.canvas.coords(raton.imagen,
                                       Animacion.esquinas(raton.x, raton.y))
        # Programar nueva llamada dentro de mseg milisegundos:
        self.ventana.canvas.after(Animacion.Mseg, self.mover )
                       
## Ubicación de los ratones

datos = """\
A 50 50 NE red
B 80 10 NO cyan
C 100 30 SE black
D 120 70 SO blue
E 15 40 N pink
F 30 65 S white
G 65 90 E brown
H 90 100 O purple
"""



def principal():
    """ Función principal del módulo.
    """
    ll = Animacion(Ventana("Animación", 200, 250), datos)
    ll.mover()

    ## Atender eventos
    ll.ventana.root.mainloop()

if __name__ == "__main__":
    principal()
    

