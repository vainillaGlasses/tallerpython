##############################################################################################
##
## Programa: 2013101697_Samantha_Arburola.py
## Fecha   : 07.11.2013
## Autor   : Samantha Arburola León
##
## Entrada : un archivo .txt con montos asignados para una inversión
##           el cual debe tener el orden:
##              codigo, provincia, canton, distrito, monto
##
## Salidas : los Distritos con cuyos códigos provinciales son erroneos,
##           el monto de inversion por provincia y el mont de inversion por canton
##
## Restricciones: entrada debe ser el string de la direccion del archivo
##
##############################################################################################


def montoDistrito(entrada):
    """ Retorna una diccionario con información de la inversión en
        millones de colones para fomentar el deporte por distrito.
        Para cada distrito se mantiene un diccionario con los siguientes campos:
             codigo    : Código del distrito (corresponde a la llave)
             valor     : Es un diccionario que a su vez tiene la siguiente
                         estructura:
                            'Provincia' : Provincia a la que pertenece el distrito
                            'Canton'    : Cantón al que pertenece el distrito
                            'Distrito'  : Nombre del distrito
                            'Monto'     : Inversión en millones de colones
             
        Entradas:
             entrada   : Nombre del archivo de entrada

             El archivo de entrada tiene la siguiente estructura:
                  Los campos vienen separados por coma (,).
                  -------------------------------------------------------
                  |CAMPO     | SIGNIFICADO                              |
                  |----------|------------------------------------------|
                  |CODELE    | Seis dígitos del código electoral        |
                  |PROVINCIA | Nombre de la provincia                   |
                  |CANTON    | Nombre del cantón electoral              |
                  |DISTRITO  | Nombre del distrito electoral            |
                  |MONTO     | Monto de la inversión (millones colones) |
                  -------------------------------------------------------

        Salidas:
            Diccionario.
            
        Restricciones:
             entrada es una tira de caracteres.

        Suposiciones :
             El archivo de entrada está bien formado.
             
    """

    ## Verifica restricciones
    assert isinstance(entrada, str) 

    ## Abre el archivo de entrada
    try:
        f = open(entrada)
    except:
        return []

    ## Genera una lista de listas a partir del archivo
    ## de entrada.
    lista = f.readlines()
    lista = [[y.strip() for y in x.split(",")] for x in lista]

    d = {}

    ## Genera una diccionario por cada lista en la lista.
    ## Incluye el diccionario en la lista distritos.
    for i in range(len(lista)):
        codigo = int(lista[i][0])
        provincia = lista[i][1]
        canton = lista[i][2]
        distrito = lista[i][3]
        monto = int(lista[i][4])
        d[codigo] = {"Provincia" : provincia,
                     "Canton" : canton,
                     "Distrito" : distrito,
                     "Monto" : monto}

    return d


##############################################################################################
dic = montoDistrito("inver.txt")
def cod_inco(entrada):
    """ Retorna una lista los Distritos con cuyos códigos provinciales son erroneos
        Entrada: diccionario
        Salida : lista de distritos
        Restriccion : la entrada debe ser un diccionario
    """
    ## Restricciones
    assert isinstance(entrada, dict)
    ## Lista Codigo de provincia
    codigo = list(dic.keys())
    ##print(codigo)

    ## Tira Codigo de provincia
    cod_str = []
    for x in codigo:
        cod_str.append(str(x))
    ##print(cod_str)
                
    ## Revisión del Código correcto para la provincia
    #print(letra_prov)
    incorrectos = []
    i = 0
    while i <= len(cod_str)-1:
        l = dic[codigo[i]]["Provincia"]
        if cod_str[i][0] == "1":
            #print(l)
            if l[0] != "S":
                incorrectos.append(dic[codigo[i]]["Distrito"])
                #print(incorrectos)
        elif cod_str[i][0] == "2":
            if l[0] != "A":
                incorrectos.append(dic[codigo[i]]["Distrito"])
                #print(incorrectos)
        elif cod_str[i][0] == "3":
            if l[0] != "C":
                incorrectos.append(dic[codigo[i]]["Distrito"])
                #print(incorrectos)
        elif cod_str[i][0] == "4":
            if l[0] != "H":
                incorrectos.append(dic[codigo[i]]["Distrito"])
                #print(incorrectos)
        elif cod_str[i][0] == "5":
            if l[0] != "G":
                incorrectos.append(dic[codigo[i]]["Distrito"])
                #print(incorrectos)
        elif cod_str[i][0] == "6":
            if l[0] != "P":
                incorrectos.append(dic[codigo[i]]["Distrito"])
                #print(incorrectos)
        elif cod_str[i][0] == "7":
            if l[0] != "L":
                incorrectos.append(dic[codigo[i]]["Distrito"])
                #print(incorrectos)
        i += 1
      
    return incorrectos
p = cod_inco(dic)
#################################################################################################           

## Suma de inversion Provincial
def sum_inv_prov(entrada):
    assert isinstance(entrada, dict)

    ## Lista Codigo de provincia
    codigo = list(dic.keys())
    ##print(codigo)

    ## Tira Codigo de provincia
    cod_str = []
    for x in codigo:
        cod_str.append(str(x))
    ##print(cod_str)

    inversion_prov = {"SAN JOSE": 0, "ALAJUELA": 0, "CARTAGO": 0, "HEREDIA": 0, "GUANACASTE": 0, "PUNTARENAS": 0, "LIMON": 0}
    i = 0
    while i <= len(codigo)-1:
        l = dic[codigo[i]]["Provincia"]
        #print(l)
        if l[0] == "S":
            monto = dic[codigo[i]]["Monto"]
            #print(monto)
            inversion_prov["SAN JOSE"] += monto
            #print(inversion_prov)
        elif l[0] == "A":
            monto = dic[codigo[i]]["Monto"]
            inversion_prov["ALAJUELA"] += monto
        elif l[0] == "C":
            monto = dic[codigo[i]]["Monto"]
            inversion_prov["CARTAGO"] += monto
        elif l[0] == "H":
            monto = dic[codigo[i]]["Monto"]
            inversion_prov["HEREDIA"] += monto
        elif l[0] == "G":
            monto = dic[codigo[i]]["Monto"]
            inversion_prov["GUANACASTE"] += monto
        elif l[0] == "P":
            monto = dic[codigo[i]]["Monto"]
            inversion_prov["PUNTARENAS"] += monto
        elif l[0] == "L":
            monto = dic[codigo[i]]["Monto"]
            inversion_prov["LIMON"] += monto
            
            
        i += 1
        
    
    return inversion_prov
    
#sup = sum_inv_prov(dic)

#######################################################################
def sum_inv_cant(entrada):
    assert isinstance(entrada, dict)
##    canton = []
##    i = 0
##    while i <= len(codigo)-1:
##        p = dic[codigo[i]]["Provincia"]
##        while p == "SAN JOSE":
##            c1 = dic[codigo[i]]["Canton"]
##            c2 = dic[codigo[i+1]["Canton"]
##            if c1 == c2:
##                     canton = str() + str(dic[codigo[i]]["Monto"]
    return "INTENTO FALLIDO"

def principal():
    entrada = input("Digite la dirección del archivo: ")
    print("Los código erroneos corresponden a los distritos:\n")
    print(cod_inco(montoDistrito("inver.txt")))
    print("\nLos monton de inversion por provincia son:\n")
    print(sum_inv_prov(montoDistrito("inver.txt")))
    print("\nLos montos de inversion por canton son:\n")
    print(sum_inv_cant(montoDistrito("inver.txt")))

if __name__ == "__main__":
	principal()


