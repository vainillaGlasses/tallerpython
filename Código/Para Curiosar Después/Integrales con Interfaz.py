# -*- coding: utf-8 -*-
# -*- coding: latin-1 -*-

"""
########################################################################
# Programa: Integra.py                                                 #
# Autores :                                                            #
#         Samantha Arburola Leon   2013101697                          #
#         Samantha Mendoza Peralta 2013016264                          #
#         Amanda Solano Astorga    2013100015                          #
# Fecha   : 05.junio.2015                                              #
#                                                                      #								   #
########################################################################
"""

""" ---------------------------------------------------------------- """
""" --------------------- Programa: Librerias ---------------------- """
## Libreria Grafica
import Tkinter
from Tkinter import *   
from Tkinter import Tk 
## Libreria Matematica
from sympy import *
## Libreria para trabajar con el str ingresado por el usuario 
from sympy.parsing.sympy_parser import parse_expr
########################################
### Definicon de Simbolos y globales ###
x = Symbol('x')
miFuncion    = ""
miPuntoA     = ""
miPuntoB     = ""
miRectangulo = ""
resultado    = 0.0
mitad        = 0.0
""" ---------------------------------------------------------------- """
""" -------------------- Programa: Sumatoria ----------------------- """ 
def particion():
    global mitad
    #Funcion que define la mitad con la que seran evaluados los rectangulos
    mitad=(miPuntoB - miPuntoA)/miRectangulo
   
def izquierda():
    #Declaramos la expresion izquierda
    global expresion_i
    expresion_i = miPuntoA +(x-1)*mitad
def derecha():
    #Declaramos la expresion derecha
    global expresion_d
    expresion_d= miPuntoA+x*mitad
    
def sumas(dato):
    """ Funcion que realiza el ciclo for de la suma """
    n = miRectangulo
    funcion = miFuncion
    #Dato es alguna de las expresiones anteriores
    #f es la variable que mostrara el resultado
    f=0
    for i in range(1,n+1):#El rango va de uno a n+1
        evaluar=dato.subs(x,i) #Evaluar es la expresion ya sea derecha o izquierda
        #evaluada en i. 
        f = f+(mitad * funcion.subs(x,evaluar))#Se suma lo que tiene f realiza la formula
        #normal
    #print round(f,7)
    return round(f,7)
""" ---------------------------------------------------------------- """
########################################################################
""" ---------------------------------------------------------------- """
""" ---------------------- Ventana Resultado ----------------------- """
""" ---------------------------------------------------------------- """
def showResultado(pMostrar):
    ## Limpiar Pantalla
    bienvenida.destroy()
    canvas = Canvas(width=700, height=520, bg="#FFFFCC") 
    #
    ################################# Muestra Resultados #################################
    resultado = Tkinter.Label(root, text = "Resultado", font = "Century 12", justify = CENTER)
    resultado.config(bg = "#FFFFCC", fg = "black")
    resultado.place(x=300, y=15)
    #
    ## Muestra Funcion
    muestraFuncion0 = Tkinter.Label(root, text= "Funcion", font = "Courier 10", justify = CENTER)
    muestraFuncion0.config(bg = "#FFFFCC", fg = "black")
    muestraFuncion0.place(x = 80, y = 50)
    #
    muestraFuncion1 = Tkinter.Label(root, text= str(miFuncion), font = "Courier 10", justify = CENTER)
    muestraFuncion1.config(bg = "#FFFFCC", fg = "black")
    muestraFuncion1.place(x = 200, y = 50)
    #
    ## Muestra Punto
    muestraPunto0 = Tkinter.Label(root, text= "Punto A", font = "Courier 10", justify = CENTER)
    muestraPunto0.config(bg = "#FFFFCC", fg = "black")
    muestraPunto0.place(x = 80, y = 75)
    #
    muestraPunto1 = Tkinter.Label(root, text= str(int(miPuntoA)), font = "Courier 10", justify = CENTER)
    muestraPunto1.config(bg = "#FFFFCC", fg = "black")
    muestraPunto1.place(x = 160, y = 75)
    #
    muestraPunto2 = Tkinter.Label(root, text= "Punto B", font = "Courier 10", justify = CENTER)
    muestraPunto2.config(bg = "#FFFFCC", fg = "black")
    muestraPunto2.place(x = 200, y = 75)
    #
    muestraPunto3 = Tkinter.Label(root, text= str(int(miPuntoB)), font = "Courier 10", justify = CENTER)
    muestraPunto3.config(bg = "#FFFFCC", fg = "black")
    muestraPunto3.place(x = 270, y = 75)
    # 
    muestra0 = Tkinter.Label(root, text= "Riemann", font = "Courier 10", justify = CENTER)
    muestra0.config(bg = "#FFFFCC", fg = "black")
    muestra0.place(x = 350, y = 50)
    #
    muestra1 = Tkinter.Label(root, text= str(pMostrar), font = "Courier 10", justify = CENTER)
    muestra1.config(bg = "#FFFFCC", fg = "black")
    muestra1.place(x = 440, y = 50)
    #
    muestraR0 = Tkinter.Label(root, text= "Rectangulo", font = "Courier 10", justify = CENTER)
    muestraR0.config(bg = "#FFFFCC", fg = "black")
    muestraR0.place(x = 350, y = 75)
    #
    muestraR1 = Tkinter.Label(root, text= str(miRectangulo), font = "Courier 10", justify = CENTER)
    muestraR1.config(bg = "#FFFFCC", fg = "black")
    muestraR1.place(x = 440, y = 75)
    #
    ## Boton Nueva Suma
    bttn_nuevaSum = Tkinter.Button(root, text= "\n     Nueva Sumatoria     \n", relief = "raised",command = sumatoriaIG)
    bttn_nuevaSum.config(bg = "#CC99FF", fg = "black")
    bttn_nuevaSum.place(x = 300, y = 350)
    #
""" ---------------------------------------------------------------- """
""" ---------------------- Ventana Principal ----------------------- """
""" ---------------------------------------------------------------- """
def sumatoriaIG():
    def fillIzquierda():
        global miFuncion
        global miPuntoA
        global miPuntoB
        global miRectangulo
        global resultado
        miFuncion    = miEntradaF.get()       
        miPuntoA     = miEntradaA.get()
        miPuntoB     = miEntradaB.get()
        miRectangulo = miEntradaR.get()
        miFuncion = parse_expr(str(miFuncion))
        miPuntoA     = int(miPuntoA)
        miPuntoB     = float(miPuntoB)
        miRectangulo = int(miRectangulo)
        datos_win.withdraw()

        particion()
        izquierda()
        resultado = sumas(expresion_i)
        showResultado(resultado)
    """ FIN """

    def fillDerecha():
        global miFuncion
        global miPuntoA
        global miPuntoB
        global miRectangulo
        global resultado
        miFuncion    = miEntradaF.get()       
        miPuntoA     = miEntradaA.get()
        miPuntoB     = miEntradaB.get()
        miRectangulo = miEntradaR.get()
        miFuncion = parse_expr(str(miFuncion))
        miPuntoA     = int(miPuntoA)
        miPuntoB     = float(miPuntoB)
        miRectangulo = int(miRectangulo)
        datos_win.withdraw()

        particion()
        derecha()
        resultado = sumas(expresion_d)
        showResultado(resultado)
    """ FIN """

    def fillAmbas():
        global miFuncion
        global miPuntoA
        global miPuntoB
        global miRectangulo
        global resultado
        res1 = 0
        res2 = 0
        
        miFuncion    = miEntradaF.get()       
        miPuntoA     = miEntradaA.get()
        miPuntoB     = miEntradaB.get()
        miRectangulo = miEntradaR.get()
        miFuncion = parse_expr(str(miFuncion))
        miPuntoA     = int(miPuntoA)
        miPuntoB     = float(miPuntoB)
        miRectangulo = int(miRectangulo)
        datos_win.withdraw()

        particion()
        izquierda()
        res1 = sumas(expresion_i)
        derecha()
        res2 = sumas(expresion_d)
        resultado = (res1 + res2) / 2
        showResultado(resultado)
    """ FIN """
    
    ## Crea la ventana Tk
    global datos_win
    datos_win = Tkinter.Toplevel(root)
    datos_win.config(bg = "#FFFFCC")
    datos_win.geometry("300x200+250+250") ## Ancho, Largo, X Y (posiciona ventana pantalla)
    datos_win.title("Integrales - Ingresar Datos")
    ## Bienvenida
    bienvenida = Tkinter.Label(datos_win, text = "Ingreso de Datos", font = "Century 12", justify = CENTER)
    bienvenida.config(bg = "#FFFFCC", fg = "black")
    bienvenida.place(x = 100, y = 10)
    ## Ingresar Funcion
    funcion = Tkinter.Label(datos_win, text = "FUNCIÓN", font = "Century 9", justify = CENTER)
    funcion.config(bg = "#FFFFCC", fg = "black")
    funcion.place(x = 10, y = 30)
    ## Cuandro para ingresar txt
    global miEntradaF
    txtFuncion = Tkinter.StringVar()
    miEntradaF = Tkinter.Entry(datos_win,textvariable = txtFuncion, width=30)
    miEntradaF.place(x = 10, y = 50)
    ## Ingresar Intervalo
    punto = Tkinter.Label(datos_win, text = "I  N  T  E  R  V  A  L  O", font = "Century 9", justify = CENTER)
    punto.config(bg = "#FFFFCC", fg = "black")
    punto.place(x = 10, y = 70)
    ## Ingresar Punto A
    punto = Tkinter.Label(datos_win, text = "[          ,          ]", font = "Century 9", justify = CENTER)
    punto.config(bg = "#FFFFCC", fg = "black")
    punto.place(x = 10, y = 95)
    ## Cuandro para ingresar txt punto A
    global miEntradaA
    txtPunto = Tkinter.StringVar()
    miEntradaA = Tkinter.Entry(datos_win,textvariable = txtPunto, width=3)
    miEntradaA.place(x = 20, y = 95)
    ## Cuandro para ingresar txt punto B
    global miEntradaB
    txtPunto = Tkinter.StringVar()
    miEntradaB = Tkinter.Entry(datos_win,textvariable = txtPunto, width=3)
    miEntradaB.place(x = 70, y = 95)
    ## Ingresar Cantidad de Rectangulos
    punto = Tkinter.Label(datos_win, text = "Rectangulos", font = "Century 9", justify = CENTER)
    punto.config(bg = "#FFFFCC", fg = "black")
    punto.place(x = 160, y = 70)
    ## Cuandro para ingresar txt rectangulos
    global miEntradaR
    txtPunto = Tkinter.StringVar()
    miEntradaR = Tkinter.Entry(datos_win,textvariable = txtPunto, width=4)
    miEntradaR.place(x = 200, y = 95)
    #
    ## Boton Calcular Izquierda
    bttn_calcular = Tkinter.Button(datos_win, text= "- Izquierda -", relief = "raised",command = fillIzquierda)
    bttn_calcular.config(bg = "#CC99FF", fg = "black")
    bttn_calcular.place(x=10,y=120)
    #
    ## Boton Calcular Derecha
    bttn_calcular = Tkinter.Button(datos_win, text= "- Derecha - ", relief = "raised",command = fillDerecha)
    bttn_calcular.config(bg = "#CC99FF", fg = "black")
    bttn_calcular.place(x=105,y=120)
    #
    ## Boton Calcular Ambas
    bttn_calcular = Tkinter.Button(datos_win, text= "  - Ambas -  ", relief = "raised",command = fillAmbas)
    bttn_calcular.config(bg = "#CC99FF", fg = "black")
    bttn_calcular.place(x=190,y=120)
    #
    ## Boton Cerrar Ventana
    bttn_cerrar = Tkinter.Button(datos_win, text= " - Cerrar  - ", relief = "raised",command = lambda:datos_win.destroy())
    bttn_cerrar.config(bg = "#CC99FF", fg = "black")
    bttn_cerrar.place(x=100,y=160)
    datos_win.transient(root)

########################################
###            Acerca de             ###
def acerca():
    """ Muestra un "acerca" del Programa"""
    ## Crea la ventana.
    global acerca_win
    acerca_win = Tkinter.Toplevel(root)
    acerca_win.geometry("200x200+200+200")
    acerca_win.config(bg = "#FFFFCC")
    acerca_win.transient(root)
    ## Informacion.
    acerca_win.title("Acerca de")
    lb_acerca  = Tkinter.Label(acerca_win, text = "Creado por\nSamantha Arburola\n&\nSamantha Mendoza\n&\nAmanda Solano")
    lb_acerca.config(bg = "#FFFFCC", fg = "black")
    lb_acerca.place(x=45, y=15)
    #
    lb_inst    = Tkinter.Label(acerca_win, text = "ITCR"+ "\n" +"Ing. en Computacion\nVersion 1.0")
    lb_inst.config(bg = "#FFFFCC", fg = "black")
    lb_inst.place(x=45, y=115)
    #
    lb_version = Tkinter.Label(acerca_win, text = "Version 1.0")
    lb_version.config(bg = "#FFFFCC", fg = "black")
    ## Boton Cerrar Ventana
    bttn_cerrar = Tkinter.Button(acerca_win, text= "Cerrar", relief = "raised",command = lambda:acerca_win.destroy())
    bttn_cerrar.config(bg = "#CC99FF", fg = "black")
    bttn_cerrar.place(x=80,y=170)
    acerca_win.transient(root)
    """ ====================== FIN  AcercaDe ======================= """    
########################################
### Mostrar las instrucciones        ###
def instructivo():
    ## Crea la ventana.
    global instructivo
    instructivo = Tkinter.Toplevel(root)
    instructivo.config(bg = "#FFFFCC")
    instructivo.title("Riemann - INSTRUCCIONES")
    instructivo.geometry("420x320+400+370") ## Ancho, Largo, X Y (posiciona ventana pantalla)
   
    
    ## Texto: Titulo.
    titulo = Tkinter.Label(instructivo, text = "INSTRUCCIONES" , font = "Impact 12")
    titulo.config(bg = "#FFFFCC", fg = "black")
    titulo.place(x = 20, y = 25)
    #
    instruccion0 = Tkinter.Label(instructivo, text= "Ingresar la funcion con los siguientes formatos", font = "Courier 8", justify = CENTER)
    instruccion0.config(bg = "#FFFFCC", fg = "black")
    instruccion0.place(x = 30, y = 50)
    #
    instruccion1 = Tkinter.Label(instructivo, text= "Suma           |  +\nResta          |  -\nDivicion       |  /\nMultiplicacion |  *\nFraccion       |  (numerador / denominador)", font = "Courier 8", justify = LEFT)
    instruccion1.config(bg = "#FFFFCC", fg = "black")
    instruccion1.place(x = 30, y = 65)
    #
    instruccion2 = Tkinter.Label(instructivo, text= "Exponencial    |  numero**exponente\nSeno           |  sin(x)\nCoseno         |  cos(x)\nTangente       |  tan(x)\nRaiz Cuadrada  |  sqrt(expresion)", font = "Courier 8", justify = LEFT)
    instruccion2.config(bg = "#FFFFCC", fg = "black")
    instruccion2.place(x = 30, y = 115)
    #
    instruccion3 = Tkinter.Label(instructivo, text= "USAR MINUSCULAS PARA ESCRIBIR LA FUNCION\n\nSe recomienda agrupar operaciones entre parentesis ()", font = "Courier 8", justify = CENTER)
    instruccion3.config(bg = "#FFFFCC", fg = "black")
    instruccion3.place(x = 30, y = 200)
    ## Boton Cerrar Ventana
    bttn_cerrar = Tkinter.Button(instructivo, text= "Cerrar", relief = "raised",command = lambda:instructivo.destroy())
    bttn_cerrar.config(bg = "#CC99FF", fg = "black")
    bttn_cerrar.place(x = 180, y = 280)
    instructivo.transient(root)
    """ ===================== FIN Instructivo ====================== """
""" ---------------------------------------------------------------- """
    
## Funcion Auxiliar para salir del programa
def salir():    
    root.destroy()
    root.quit()
## Ventana principal y propiedades
root = Tk()
root.title("Riemann")
root.geometry("720x520+150+150")
root.config(bg = "#FFFFCC")
## Barra de menu
menubar = Menu(root)
## SubMenu Derivar
filemenu = Menu(menubar, tearoff = 0)
filemenu.add_command(label = "Ingresar Integral", command = sumatoriaIG)
filemenu.add_command(label = "Cerrar", command = salir)
menubar.add_cascade(label  = "Sumatoria", menu = filemenu)
## SubMenu Ayuda
helpmenu = Menu(menubar, tearoff = 0)
helpmenu.add_command(label = "Intrucciones", command = instructivo)
helpmenu.add_command(label = "Acerca de...", command = acerca)
menubar.add_cascade(label  = "Ayuda", menu = helpmenu)
## Mensaje Bienvenida
global bienvenida
bienvenida = Tkinter.Label(root, text= " - ' ' ' - . . . - ' ' ' - . . . - \nBienvenido-a\n\nSumatorias Riemann\n - . . . - ' ' ' - . . . - ' ' ' - ", font = "Courier 20", justify = CENTER)
bienvenida.config(bg = "#FFFFCC", fg = "black")
bienvenida.place(x = 75, y = 150)
root.config(menu=menubar)

root.mainloop()

##inicio()
